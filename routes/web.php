<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function() {
    return view('home');
})->name('home');

Route::get('/personas', 'PersonaController@index')->name('personas.index');
Route::get('/personas/crear', 'PersonaController@create')->name('personas.create');

Route::get('/personas/{persona}/editar', 'PersonaController@edit')->name('personas.edit');
Route::patch('/personas/{persona}', 'PersonaController@update')->name('personas.update');

Route::post('/personas', 'PersonaController@store')->name('personas.store');
Route::get('/personas/{persona}', 'PersonaController@show')->name('personas.show');

Route::delete('/personas/{persona}', 'PersonaController@destroy')->name('personas.destroy');

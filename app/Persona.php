<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $fillable = ['cedula', 'nombre', 'apellido', 'ciudad'];

    protected $primaryKey = 'cedula';
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'cedula';
    }
}
